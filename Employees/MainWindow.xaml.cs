﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Division> division;
        ObservableCollection<Employee> employee = new ObservableCollection<Employee>();
        public MainWindow()
        {
            InitializeComponent();
            {
                ObservableCollection<Employee> employee;
                division = new ObservableCollection<Division>();
                Division firstDivision= new Division()
                {
                    NameOfDivision = "Legal department"
                };
                Division secondDivision = new Division()
                {
                    NameOfDivision = "Bookeeping"
                };

                employee = new ObservableCollection<Employee>
                {
                   new Employee
                   {
                    Id = 1,
                    FirstName = "Assel",
                    LastName = "Baimukhanbetova",
                    Position = "accountant",
                    Division = firstDivision
                   },

                   new Employee
                   {
                    Id = 2,
                    FirstName = "Alexey",
                    LastName = "Petrov",
                    Position = "lawyer",
                    Division=secondDivision
                   }
                };
                employeeGrid.ItemsSource = employee;
            }
        }
    }
}
