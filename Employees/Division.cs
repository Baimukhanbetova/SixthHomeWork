﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    public class Division : INotifyPropertyChanged
    {
        private string nameOfDivision;
        public string NameOfDivision
        {
            get { return nameOfDivision; }
            set
            {
                nameOfDivision = value;
                OnPropertyChanged("nameOfDivision");
            }
        }

        public override string ToString()
        {
            return nameOfDivision;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string property = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
